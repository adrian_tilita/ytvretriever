Composer.json install

"require": {
    [..]
    "ytvretriever" : "dev-master"
}

"repositories" : [
    [...]
    {
        "type" : "vcs",
        "url" : "https://adrian_tilita@bitbucket.org/adrian_tilita/ytvretriever.git"
    }
]

Register bundle in AppKernel.php

$bundles = array(
    [...]
    new YouTubeVideoRetrieverBundle\YouTubeVideoRetrieverBundle(),
);

Clear Cache